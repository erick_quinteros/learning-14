import pandas as pd
import numpy as np
import warnings
from datetime import datetime, timedelta

from common.pyUtils.logger import get_module_logger
from common.pyUtils.database_config import DatabaseConfig
from common.pyUtils.database_utils import other_database_op, check_if_column_exists, check_if_table_exists
from anchorAccuracy.utils.AnchorTableNamesConfig import AnchorTableNamesConfig


logger = get_module_logger(__name__)


class AccuracyReportDataLoader:
    """
    parent class for all data loading object
    """

    col_type = {}

    def __init__(self, conn_pool, start_date, end_date, is_nightly=True):

        self.start_date = start_date
        self.end_date = end_date
        self.conn_pool = conn_pool
        self.is_nightly = is_nightly
        # compose date_filter
        self.date_filter = self._compose_date_filter()
        # db config from DatabaseConfig
        self.dbconfig = DatabaseConfig.instance()
        # default value for attribute
        self.load_sql = None
        self.loaded_data = pd.DataFrame()
        # input anchor tables target for data loading
        self.table_config = AnchorTableNamesConfig.instance()

    def _compose_date_filter(self):
        if self.start_date is not None and self.end_date is not None:
            date_filter = '''BETWEEN {!r} AND {!r} '''.format(self.start_date, self.end_date)
        elif self.start_date is not None:
            date_filter = '''>= {!r}'''.format(self.start_date)
        elif self.end_date is not None:
            date_filter = '''<= {!r}'''.format(self.end_date)
        else:
            date_filter = None
        return date_filter

    def _check_date_filter(self):
        if self.date_filter is None:
            raise ValueError("no date filter, check input params")

    def _compose_load_sql(self):
        # will be overide by child class
        pass

    def _check_if_load_table_exists(self):
        return True

    def load(self):
        if not self._check_if_load_table_exists():
            return pd.DataFrame()
        self._compose_load_sql()
        logger.debug("Loading using {}: {}".format(self.__class__.__name__, self.load_sql))
        self.loaded_data = pd.read_sql(self.load_sql, self.conn_pool.get_engine(), parse_dates=['date']).astype(self.col_type)
        logger.info('Finish loading using {}: dim=({})'.format(self.__class__.__name__, ",".join("{}".format(i) for i in self.loaded_data.shape)))
        return self.loaded_data


class AccuracyReportVisitDataLoader(AccuracyReportDataLoader):
    """
    child class for loading visit data
    """

    col_type = {'repId':np.int64, "facilityId":np.int64, "latitude":np.float64, "longitude":np.float64,
                'date':'datetime64[ns]','accountId':np.int64}

    def __init__(self, conn_pool, start_date, end_date):
        super().__init__(conn_pool, start_date, end_date)

    def _compose_load_sql(self):
        self._check_date_filter()
        self.load_sql = '''SELECT DISTINCT r.repId, ra.accountId, r.facilityId, Fa.latitude, Fa.longitude, r.startDateLocal as date FROM {dbname_dse}.Interaction r JOIN {dbname_dse}.InteractionAccount ra ON r.interactionId=ra.interactionId JOIN {dbname_dse}.Facility Fa ON r.facilityId = Fa.facilityId WHERE {date_filter} r.interactionTypeId IN (1,4,7,10) AND r.repActionTypeId IN(2,3,4,5) AND r.isCompleted = 1 AND r.isDeleted = 0;'''.format(dbname_dse=self.dbconfig.dbname_dse, date_filter='''r.startDateLocal ''' + self.date_filter + ''' AND''')
        #self.load_sql = '''SELECT DISTINCT r.repId, r.facilityId, Fa.latitude, Fa.longitude, r.startDateLocal as date FROM {dbname_dse}.Interaction r JOIN {dbname_dse}.Facility Fa ON r.facilityId = Fa.facilityId WHERE r.interactionTypeId IN (1,4,7,10) AND r.repActionTypeId IN(2,3,4,5) AND r.isCompleted = 1 AND r.isDeleted = 0;'''.format(dbname_dse=self.dbconfig.dbname_dse)


class AccuracyReportSuggestDataLoader(AccuracyReportDataLoader):
    """
    child class for loading suggest data
    """

    col_type = {'repId':np.int64, 'facilityId':np.int64, 'accountId':np.int64, 'latitude':np.float64, 'longitude':np.float64, 'date':'datetime64[ns]'}

    def __init__(self, conn_pool, start_date, end_date):
        super().__init__(conn_pool, start_date, end_date)

    def _compose_load_sql(self):
        self._check_date_filter()
        self.load_sql = '''SELECT DISTINCT Sug.repId, Sug.accountId, Sug.facilityId, Sug.latitude, Sug.longitude, Sug.suggestedDate as date FROM (SELECT rrd.repId, rrd.suggestedDate, rrda.accountId, rrda.accountUID, a.facilityId, Fa.latitude, Fa.longitude FROM {dbname_dse}.DSERun r JOIN {dbname_dse}.DSERunRepDate rrd USING(runId) JOIN {dbname_dse}.DSERunRepDateSuggestion rrda USING (runRepDateId)JOIN {dbname_dse}.Account a USING (accountId) JOIN {dbname_dse}.Facility Fa ON a.facilityId = Fa.facilityId WHERE {date_filter} rrda.suggestedRepActionTypeUID ='VISIT') Sug INNER JOIN {dbname_cs}.Address_vod__c adr ON Sug.accountUID = adr.Account_vod__C AND adr.Primary_vod__c = 1;'''.format(dbname_dse=self.dbconfig.dbname_dse, dbname_cs=self.dbconfig.dbname_cs, date_filter='''r.`startDateLocal` ''' + self.date_filter + ''' AND''')
        #self.load_sql = '''SELECT DISTINCT Sug.repId, Sug.accountID, Sug.facilityId, Sug.latitude, Sug.longitude, Sug.suggestedDate as date FROM (SELECT rrd.repId, rrd.suggestedDate, rrda.accountId, rrda.accountUID, a.facilityId, Fa.latitude, Fa.longitude FROM {dbname_dse}.DSERun r JOIN {dbname_dse}.DSERunRepDate rrd USING(runId) JOIN {dbname_dse}.DSERunRepDateSuggestion rrda USING (runRepDateId)JOIN {dbname_dse}.Account a USING (accountId) JOIN {dbname_dse}.Facility Fa ON a.facilityId = Fa.facilityId WHERE rrda.suggestedRepActionTypeUID ='VISIT') Sug INNER JOIN pfizerprod_cs.Address_vod__c adr ON Sug.accountUID = adr.Account_vod__C AND adr.Primary_vod__c = 1;'''.format(dbname_dse=self.dbconfig.dbname_dse)


class AccuracyReportPredictDataLoader(AccuracyReportDataLoader):
    """
    child class for loading predict data
    """

    col_type = {'repId':np.int64, 'predict_latitude':np.float64, 'predict_longitude':np.float64, 'date':'datetime64[ns]', 'maxNearDistance':np.float64}

    def __init__(self, conn_pool, is_nightly, start_date=None, end_date=None):
        super().__init__(conn_pool, start_date, end_date, is_nightly)

    def _compose_load_sql(self):
        if self.is_nightly:
            self.load_sql = '''SELECT repId, date, latitude as predict_latitude, longitude as predict_longitude, maxNearDistance FROM {RepDateLocation_nightly_schema}.{RepDateLocation_nightly_tableName};'''
            self.load_sql = self.load_sql.format(RepDateLocation_nightly_schema=self.dbconfig.schema_mapping[self.table_config.RepDateLocation_nightly['schema']], RepDateLocation_nightly_tableName=self.table_config.RepDateLocation_nightly['tableName'])
        else:
            self._check_date_filter()
            self.load_sql = '''SELECT arc.repId, arc.date, arc.`latitude` as predict_latitude, arc.`longitude` as predict_longitude, NULL as maxNearDistance FROM {RepDateLocation_nightly_arc_schema}.{RepDateLocation_nightly_arc_tableName} arc INNER JOIN (SELECT min(DATE) `date`, repId, arcDate FROM {RepDateLocation_nightly_arc_schema}.{RepDateLocation_nightly_arc_tableName} WHERE DATE_ADD(arcDate, INTERVAL 1 DAY) = DATE AND DATE {date_filter} GROUP BY repId, arcDate) Prediction ON Prediction.date = arc.date AND Prediction.arcDate = arc.arcDate AND Prediction.repId=arc.repId WHERE arc.date {date_filter};'''.format(RepDateLocation_nightly_arc_schema=self.dbconfig.schema_mapping[self.table_config.RepDateLocation_nightly_arc['schema']], RepDateLocation_nightly_arc_tableName=self.table_config.RepDateLocation_nightly_arc['tableName'], date_filter=self.date_filter)
            #self.load_sql = '''SELECT arc.repId, arc.date, arc.`latitude` as predict_latitude, arc.`longitude` as predict_longitude FROM {dbname_stage}.RepDateLocation_arc arc INNER JOIN (SELECT min(DATE) `date`, repId, arcDate FROM {dbname_stage}.RepDateLocation_arc WHERE DATE_ADD(arcDate, INTERVAL 1 DAY) = DATE GROUP BY repId, arcDate) Prediction ON Prediction.date = arc.date AND Prediction.arcDate = arc.arcDate AND Prediction.repId=arc.repId;'''.format(dbname_stage=self.dbconfig.dbname_stage)


class AccuracyReportPredictFacilityDataLoader(AccuracyReportDataLoader):
    """
    child class for loading predict facility data
    """

    col_type = {'repId':np.int64, 'latitude':np.float64, 'longitude':np.float64, 'date':'datetime64[ns]', 'probability':np.float64, 'facilityId':np.int64}

    def __init__(self, conn_pool, is_nightly, start_date, end_date):
        super().__init__(conn_pool, start_date, end_date, is_nightly)

    def _compose_load_sql(self):
        if self.is_nightly:
            self._check_date_filter()
            self.load_sql = '''SELECT repId, date, facilityId, latitude, longitude, probability FROM {RepDateFacility_nightly_schema}.{RepDateFacility_nightly_tableName} where date {date_filter};'''.format(RepDateFacility_nightly_schema=self.dbconfig.schema_mapping[self.table_config.RepDateFacility_nightly['schema']], RepDateFacility_nightly_tableName=self.table_config.RepDateFacility_nightly['tableName'], date_filter=self.date_filter)
        else:
            self._check_date_filter()
            self.load_sql = '''SELECT arc.repId, arc.date, arc.facilityId, arc.`latitude`, arc.`longitude`, arc.probability FROM {RepDateFacility_nightly_arc_schema}.{RepDateFacility_nightly_arc_tableName} arc INNER JOIN (SELECT min(DATE) `date`, repId, arcDate FROM {RepDateFacility_nightly_arc_schema}.{RepDateFacility_nightly_arc_tableName} WHERE DATE_ADD(arcDate, INTERVAL 1 DAY) = DATE AND DATE {date_filter} GROUP BY repId, arcDate) Prediction ON Prediction.date = arc.date AND Prediction.arcDate = arc.arcDate AND Prediction.repId=arc.repId WHERE arc.date {date_filter};'''.format(RepDateFacility_nightly_arc_schema=self.dbconfig.schema_mapping[self.table_config.RepDateFacility_nightly_arc['schema']], RepDateFacility_nightly_arc_tableName=self.table_config.RepDateFacility_nightly_arc['tableName'], date_filter=self.date_filter)

    def _check_if_load_table_exists(self):
        if self.is_nightly:
            return check_if_table_exists(self.dbconfig.schema_mapping[self.table_config.RepDateFacility_nightly['schema']], self.table_config.RepDateFacility_nightly['tableName'], self.conn_pool)
        else:
            return check_if_table_exists(self.dbconfig.schema_mapping[self.table_config.RepDateFacility_nightly_arc['schema']], self.table_config.RepDateFacility_nightly_arc['tableName'], self.conn_pool)


class AccuracyReportDataLoaderDriver:
    """
    driver class to take care of all related data loading for accuracy report
    """

    def __init__(self, conn_pool, is_nightly, start_date=None, end_date=None):
        self.start_date = start_date
        self.end_date = end_date
        self.conn_pool = conn_pool
        self.is_nightly = is_nightly
        self.dbconfig = DatabaseConfig.instance()
        # return results
        self.predict_df = None
        self.visit_df = None
        self.suggest_df = None
        self.predict_facility_df = None
        self.nightly_prediction_count = None
        # input anchor tables target for data loading
        self.table_config = AnchorTableNamesConfig.instance()

    def _validate_and_adjust_dates(self):
        logger.info("validating dates")
        # # adjust end_date to be the min of end_date, runtime date, and min date in accuracyReport with source='nightly'
        # date_query = '''SELECT MIN(`date`) AS `date` FROM  {dbname_learning}.RepDateLocationAccuracy WHERE `source` = 'nightly';'''.format(dbname_learning=self.dbconfig.dbname_learning)
        # return_cursor = other_database_op(date_query, self.conn_pool, return_cursor=True)
        # first_nightly_date = return_cursor.fetchone()[0]
        # temp_end_date = datetime.now().date() if pd.isnull(first_nightly_date) else min(first_nightly_date, datetime.now().date())
        # temp_end_date = (temp_end_date - timedelta(days=1)).strftime("%Y-%m-%d") #use mysql BETWEEN (inc) for dates
        # if self.end_date > temp_end_date:
        #     self.end_date = temp_end_date
        #     warnings.warn("Invalid end date. End date being reset to the minimum of the earliest nightly date in accuracy report and the runtime date")
        if self.start_date > self.end_date:
            raise ValueError("start_date={}, end+date={}: start date can not be greater than end date. decrease end date param".format(self.start_date, self.end_date))
        logger.info("done validating dates, start_date={}, end_date={}".format(self.start_date, self.end_date))

    def _load_and_log_nightly_run_runUID(self):
        learningRunUID_exists = check_if_column_exists(self.dbconfig.schema_mapping[self.table_config.RepDateLocation_nightly['schema']], self.table_config.RepDateLocation_nightly['tableName'], "learningRunUID", self.conn_pool)
        if learningRunUID_exists:
            date_query = '''SELECT learningRunUID FROM {}.{} limit 1;'''.format(self.dbconfig.schema_mapping[self.table_config.RepDateLocation_nightly['schema']], self.table_config.RepDateLocation_nightly['tableName'])
            return_cursor = other_database_op(date_query, self.conn_pool, return_cursor=True)
            self.nightly_run_uid = return_cursor.fetchone()[0]
            logger.info("Run Nihgtly Anchor Accuracy Report for anchor RUN_UID={}".format(self.nightly_run_uid))
        else:
            self.nightly_run_uid = None
            logger.info("Run Nihgtly Anchor Accuracy Report for anchor without learningRunUID column")

    def _load_nightly_build_and_version(self):
        if self.nightly_run_uid is not None:
            nightly_build_query = '''SELECT learningBuildUID, learningVersionUID FROM {dbname_learning}.LearningRun WHERE learningRunUID="{learningRun}"'''.format(
                dbname_learning=self.dbconfig.dbname_learning, learningRun=self.nightly_run_uid)
            return_cursor = other_database_op(nightly_build_query, self.conn_pool, return_cursor=True)
            build_version_pair = return_cursor.fetchone()
            self.nightly_build_uid = build_version_pair[0]
            self.nightly_version_uid = build_version_pair[1]
            logger.info('Finish loading nightly run version & build info: build_UID={}, VERSION_UID={}'.format(self.nightly_build_uid, self.nightly_version_uid))
        else:
            self.nightly_build_uid = None
            self.nightly_version_uid = None
            logger.info("Run Nihgtly Anchor Accuracy Report for anchor without learningRunUID column, no associated buildUID & versionUID")

    def _validate_nightly_load_start_end_date(self):
        if self.predict_df.shape[0] > 0:
            self.start_date = min(self.predict_df.date).strftime("%Y-%m-%d")
            self.end_date = min(max(self.predict_df.date), datetime.now()).strftime("%Y-%m-%d")
            self.predict_df = self.predict_df[self.predict_df.date <= self.end_date]
            logger.info('After processing predict data by correcting end_date: dim=({})'.format(",".join(
                "{}".format(i) for i in self.predict_df.shape)))
        else:
            logger.error("predict data is empty for nightly run")
            raise RuntimeError("predict data is empty for nightly run")

    def run(self):
        logger.info("Start Loading Data for is_nightly={}, start_date={}, end_date={}".format(self.is_nightly, self.start_date, self.end_date))

        # valid start & end date for history run
        if not self.is_nightly: # validate
            self._validate_and_adjust_dates()

        # load predict first
        self.predict_df = AccuracyReportPredictDataLoader(self.conn_pool, self.is_nightly, self.start_date, self.end_date).load()
        # if nightly report, get start_date & end_date from the loaded predict data (end_date will be max of what in predict if it is not larger than current date
        if self.is_nightly:
            self._validate_nightly_load_start_end_date()
            self.nightly_prediction_count = self.predict_df.shape[0]
            self._load_and_log_nightly_run_runUID()
            self._load_nightly_build_and_version()
        self.predict_facility_df = AccuracyReportPredictFacilityDataLoader(self.conn_pool, self.is_nightly, self.start_date, self.end_date).load()

        # load visit & suggest
        self.visit_df = AccuracyReportVisitDataLoader(self.conn_pool, self.start_date,self.end_date).load()
        self.suggest_df = AccuracyReportSuggestDataLoader(self.conn_pool, self.start_date,self.end_date).load()

        logger.info("Finish Loading Data for is_nightly={}, start_date={}, end_date={}".format(self.is_nightly, self.start_date,self.end_date))
        return self
