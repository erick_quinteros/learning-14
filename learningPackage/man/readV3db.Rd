\name{readV3db}
\alias{readV3db}
%- Also NEED an '\alias' for EACH other topic documented here.
\title{readV3db}
\description{
%%  ~~ A concise (1-5 lines) description of what the function does. ~~
}
\usage{
readV3db(u, p, h, schema)
}
%- maybe also 'usage' for other objects documented here.
\arguments{
  \item{u}{
%%     ~~Describe \code{u} here~~
}
  \item{p}{
%%     ~~Describe \code{p} here~~
}
  \item{h}{
%%     ~~Describe \code{h} here~~
}
  \item{schema}{
%%     ~~Describe \code{schema} here~~
}
}
\details{
%%  ~~ If necessary, more details than the description above ~~
}
\value{
%%  ~Describe the value returned
%%  If it is a LIST, use
%%  \item{comp1 }{Description of 'comp1'}
%%  \item{comp2 }{Description of 'comp2'}
%% ...
}
\references{
%% ~put references to the literature/web site here ~
}
\author{
%%  ~~who you are~~
}
\note{
%%  ~~further notes~~
}

%% ~Make other sections like Warning with \section{Warning }{....} ~

\seealso{
%% ~~objects to See Also as \code{\link{help}}, ~~~
}
\examples{
##---- Should be DIRECTLY executable !! ----
##-- ==>  Define data, use random,
##--	or do  help(data=index)  for the standard data sets.

## The function is currently defined as
function (u, p, h, schema) 
{
    library(RMySQL)
    library(data.table)
    flog.info("saveData \%s \%s \%s \%s", u, p, h, schema)
    drv <- dbDriver("MySQL")
    if (is.na(CPORT)) 
        con <- dbConnect(drv, user = u, password = p, host = h)
    else con <- dbConnect(drv, user = u, password = p, host = h, 
        po = CPORT)
    dbGetQuery(con, "SET NAMES utf8")
    accounts <- data.table(dbGetQuery(con, paste("SELECT * FROM ", 
        paste(schema, ".Account", sep = ""), " WHERE isDeleted=0;")))
    reps <- data.table(dbGetQuery(con, paste("SELECT r.repId, r.repName, rtr.repTeamName FROM ", 
        paste(schema, ".Rep", sep = ""), " as r JOIN ", paste(schema, 
            ".RepTeamRep", sep = ""), " using (repId) JOIN ", 
        paste(schema, ".RepTeam", sep = ""), " rtr using (repTeamId) WHERE isActivated=1;")))
    repAssignments <- data.table(dbGetQuery(con, paste("SELECT repId, accountId FROM ", 
        paste(schema, ".RepAccountAssignment;", sep = ""))))
    accts <- unique(repAssignments[repId \%in\% reps$repId]$accountId)
    facilities <- data.table(dbGetQuery(con, paste("SELECT facilityId, externalId, facilityName, geoLocationString, latitude, longitude FROM ", 
        paste(schema, ".Facility;", sep = ""))))
    accountProduct <- data.table(dbGetQuery(con, paste("SELECT ap.*, p.productName FROM ", 
        paste(schema, ".AccountProduct as ap", sep = ""), " JOIN ", 
        paste(schema, ".Product", sep = ""), "p using (productId);")))
    accountProduct$productId <- NULL
    accountProduct <- accountProduct[accountId \%in\% accts]
    accounts <- accounts[accountId \%in\% accts]
    targets <- data.table(dbGetQuery(con, paste("SELECT tc.startDate, tc.endDate, s.repId, s.accountId, s.target, tl.targetingLevelName, ra.repActionTypeName, p.productName FROM ", 
        paste(schema, ".StrategyTarget", sep = ""), " as s JOIN ", 
        paste(schema, ".Product", sep = ""), " p using (productId) JOIN ", 
        paste(schema, ".TargetingLevel", sep = ""), " tl using (targetingLevelId) JOIN ", 
        paste(schema, ".RepActionType", sep = ""), " ra using (repActionTypeId) JOIN ", 
        paste(schema, ".TargetsPeriod", sep = ""), "tc using (targetsPeriodId);")))
    interactions <- data.table(dbGetQuery(con, paste("SELECT  v.repId, v.facilityId, v.wasCreatedFromSuggestion, ia.accountId, vt.interactionTypeName, vp.messageId, pi.productInteractionTypeName, p.productName, v.duration, v.startDateLocal\n                      FROM ", 
        paste(schema, ".Interaction", sep = ""), " as v JOIN ", 
        paste(schema, ".InteractionType", sep = ""), " vt using (interactionTypeId) JOIN ", 
        paste(schema, ".InteractionProduct", sep = ""), " vp using (interactionId) JOIN ", 
        paste(schema, ".ProductInteractionType", sep = ""), " pi using (productInteractionTypeId) JOIN ", 
        paste(schema, ".Product", sep = ""), " p using (productId) JOIN ", 
        paste(schema, ".InteractionAccount", sep = ""), " ia using (interactionId) WHERE v.isCompleted=1 AND v.isDeleted=0;")))
    tmp <- paste("SELECT  e.repId, e.accountId, e.eventDate, et.eventTypeName, e.eventLabel, e.eventDateTimeUTC, mi.messageName, mi.messageDescription, p.productName FROM ", 
        paste(schema, ".Event", sep = ""), " as e JOIN ", paste(schema, 
            ".EventType", sep = ""), " et using (eventTypeId) JOIN ", 
        paste(schema, ".Message", sep = ""), " mi using (messageId) JOIN ", 
        paste(schema, ".Product", sep = ""), " p ON p.productId=e.productId;")
    events <- data.table(dbGetQuery(con, tmp))
    products <- data.table(dbGetQuery(con, sprintf("SELECT productName, externalId FROM \%s.Product WHERE isActive=1 and isDeleted=0;", 
        schema)))
    DSEconfig <- data.table(dbGetQuery(con, sprintf("SELECT seConfigId, seConfigName, description, configSpecjson FROM \%s.DSEConfig;", 
        schema)))
    interactions <- interactions[accountId \%in\% accts]
    interactions$date <- as.Date(interactions$startDateLocal)
    interactions$startDateLocal <- NULL
    interactions[, `:=`(c("interactionTypeName", "productInteractionTypeName", 
        "productName"), list(as.factor(interactionTypeName), 
        as.factor(productInteractionTypeName), as.factor(productName)))]
    messages <- data.table(dbGetQuery(con, paste("Select m.messageId, m.messageName, m.messageDescription, p.productName FROM ", 
        paste(schema, ".Message ", sep = ""), "as m JOIN ", paste(schema, 
            ".Product ", sep = ""), "p using(productId);")))
    dbDisconnect(con)
    return(list(accounts = accounts, reps = reps, repAssignments = repAssignments, 
        facilities = facilities, targets = targets, interactions = interactions, 
        messages = messages, accountProduct = accountProduct, 
        events = events, products = products))
  }
}
% Add one or more standard keywords, see file 'KEYWORDS' in the
% R documentation directory.
\keyword{ ~kwd1 }% use one of  RShowDoc("KEYWORDS")
\keyword{ ~kwd2 }% __ONLY ONE__ keyword per line
