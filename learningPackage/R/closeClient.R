##########################################################
#
#
# aktana-learning Install Aktana Learning Engines.
#
# description: stop redirect logging and print to console
#
#
# created by : shirley.xu@aktana.com
#
# created on : 2015-10-13
#
# Copyright AKTANA (c) 2015.
#
#
##########################################################

closeClient <- function()
{
  suppressWarnings(sink(NULL))
  flog.appender(appender.console())
  options(error=NULL)
}