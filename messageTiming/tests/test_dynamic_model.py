import pytest

import sys
sys.path.append("../")

import pandas as pd

from build_dynamic_model import build_dynamic_model

VAL_NO_DATA = 999 

def f():
    raise SystemExit(1)

def test_mytest():
    with pytest.raises(SystemExit):
        f()

def test_dynamic_model():
    """
    Usage: pytest -v -s  # print out input and output
    
    """
    pd_data = dict(accountId=[1002,1002,1002,1002,1002,1002,1002,1002,1002,1002,1002,1003,1003,1003,1003],
                   type=['SEND', 'SEND', 'SEND', 'SEND', 'VISIT', 'TARGET', 'TARGET', 'SEND', 'VISIT', 'VISIT', 'TARGET',
                     'SEND', 'SEND', 'VISIT', 'TARGET'],
                   date=["2018-06-01", "2018-07-03", "2018-07-06", "2018-07-10", "2018-07-13", "2018-07-14", "2018-07-15",
                     "2018-07-19", "2018-07-21", "2018-07-26", "2018-07-29", "2018-07-02", "2018-07-10", "2018-07-13",
                     "2018-07-14"])

    df = pd.DataFrame(pd_data, columns=['accountId', 'type', 'date'])
    df['date'] = df['date'].astype('datetime64[ns]')

    print("\nInput data looks like:")
    print(df)

    design = build_dynamic_model(df)

    print("\nOutput model looks like:")
    print(design)

    assert design.at[1, 'preSEND']   == 32
    assert design.at[1, 'preVISIT']   == VAL_NO_DATA
    assert design.at[2, 'TARGET'] == 1.0
    assert design.at[3, 'numS']   == 4
    assert design.at[4, 'numV']   == 1
    assert design.at[4, 'event']  == 'V'
    assert design.at[4, 'TARGET'] == 0.75
    assert design.at[7, 'preSEND']   == 9
    assert design.at[7, 'preTARGET']   == 4
    assert design.at[7, 'pre2S']  == 13
    assert design.at[7, 'TARGET'] == 1.0
    assert design.at[8, 'TARGET'] == 0.75
    assert design.at[8, 'preVISIT']   == 8
    assert design.at[8, 'numT']   == 2
    assert design.at[9, 'pre2S']   == 16
    assert design.at[12, 'TARGET'] == 1.0
    assert design.at[12, 'preSEND']   == 8
    assert design.at[12, 'numT']   == 0
    assert design.at[13, 'TARGET'] == 0.5
    assert design.at[13, 'preVISIT']   == VAL_NO_DATA

