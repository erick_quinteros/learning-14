context('test the messageTimingScoreDriver script in the sparkMessageTiming module')
print(Sys.time())

# load library, loading common source scripts
library(uuid)
library(openxlsx)
library(Learning)
library(sparkLearning)
library(sparklyr)
library(dplyr)

source(sprintf("%s/common/dbConnection/dbConnection.R",homedir))
source(sprintf('%s/common/unitTest/customExpectFunc.r',homedir))
source(sprintf('%s/common/unitTest/utilsFunc.r',homedir))

libdir <<- sprintf("%s/library", homedir)

# build used for testing
buildUID <- readModuleConfig(homedir, 'messageTiming','buildUID')
added_buildUID <- readModuleConfig(homedir, 'messageTiming','buildUID_nightlyscoreadd')
CATALOGENTRY <<- " "
BUILD_UID <<- buildUID

######################## func for reset mock data for run messageScoreDriver ###############################
runMessageTimingScoreDriverMockDataReset <- function(isNightly, buildUID) {
  # writing mock data to database (func in source(sprintf('%s/common/unitTest/initializeUnitTest.r',homedir)))
  requiredMockDataList <- list(pfizerusdev=c('Product','RepTeam','Message','MessageSetMessage','Event','EventType','Interaction','InteractionType','ProductInteractionType','InteractionProduct','InteractionAccount','InteractionProductMessage','AccountProduct','Rep','RepTeamRep','RepAccountAssignment','RepActionType','Account','MessageSet','TimeToEngageAlgorithm','AccountTimeToEngage'),pfizerusdev_learning=c('LearningFile','LearningBuild','LearningRun', 'AccountTimeToEngage', 'AccountTimeToEngageReport','SegmentTimeToEngageReport','TTEhcpReport','TTEsegmentReport','AKT_Message_Topic_Email_Learned','Sent_Email_vod__c_arc'),pfizerprod_cs=c('Approved_Document_vod__c','RecordType', 'Call2_Sample_vod__c'))

  resetMockData(homedir,dbuser,dbhost,port,dbpassword,dbname,dbname_cs,requiredMockDataList)

  # add mock build dir (func in source(sprintf('%s/common/unitTest/initializeUnitTest.r',homedir)))
  setupMockBuildDir(homedir, 'messageTiming', buildUID, forNightly=isNightly, updateModelSaveFileSheetNo=list(2,4,6))

  # copy the build folder using new BUILD_UID to make sure the loop algorithm of nightly scoring works
  if (isNightly) {
    new_buildUID <- readModuleConfig(homedir, 'messageTiming','buildUID_nightlyscoreadd')
    new_versionUID <- readModuleConfig(homedir, 'messageTiming','versionUID_nightlyscoreadd')
    new_configUID <- readModuleConfig(homedir, 'messageTiming','configUID_nightlyscoreadd')
    setupMockBuildDir (homedir, 'messageTiming', buildUID, files_to_copy=NULL, forNightly=TRUE, new_BUILD_UID=new_buildUID, new_configUID=new_configUID, new_versionUID=new_versionUID, updateModelSaveFileSheetNo=list(2,4,6))
  }
}

######################## func for run messageScoreDriver ##########################################
runMessageTimingScoreDriver <- function(isNightly, buildUID) {
  # fix scoreDate so that the result is always the same
  scoreDate <<- as.Date(readModuleConfig(homedir, 'messageTiming','scoreDate'))

  if (!isNightly) {
    # arguments needed for manual scoring
    RUN_UID <<- UUIDgenerate()
    BUILD_UID <<- buildUID

    # add entry to learningRun (simulate what done by API before calling the R script) (for manual scoring only)
    config <- initializeConfigurationNew(homedir, BUILD_UID)
    con <- getDBConnectionLearning(dbuser, dbpassword, dbhost, dbname, port)
    now <- format(Sys.time(), "%Y-%m-%d %H:%M:%S")

    VERSION_UID <<- config[["versionUID"]]
    CONFIG_UID  <<- config[["configUID"]]

    SQL <- sprintf("INSERT INTO LearningRun (learningRunUID,learningBuildUID,learningVersionUID,learningConfigUID,isPublished,runType,executionStatus,executionDateTime) VALUES('%s','%s','%s','%s',0,'TTE','running','%s');",RUN_UID, BUILD_UID, VERSION_UID, CONFIG_UID, now)

    dbClearResult(dbSendQuery(con, SQL))
    dbDisconnect(con)
  } else {
    if (exists("BUILD_UID", envir=globalenv())) {rm(BUILD_UID, envir=globalenv())}
    if (exists("RUN_UID", envir=globalenv())) {rm(RUN_UID, envir=globalenv())}
  }

  sc <<- initializeSpark(homedir, master="local", version="2.3.1")

  # run messageScoreDriver
  source(sprintf('%s/sparkMessageTiming/messageTimingScoreDriver.r',homedir))

  return (RUN_UID)
}

############################# main test script ########################################################

# test cases

test_that('test messageTimingScoreDriver for nighlty scoring', {
  # run script
  isNightly <- TRUE
  runmodel <<- " "

  runMessageTimingScoreDriverMockDataReset(isNightly, buildUID)

  numOfFilesInBuildFolder <- checkNumOfFilesInFolder(sprintf('%s/builds/%s',homedir,buildUID))

  # Run score driver
  runUID <- runMessageTimingScoreDriver(isNightly, buildUID)

  # test cases
  # load saved nightly scoring for comparison
  load(sprintf('%s/messageTiming/tests/data/from_DSE_AccountTimeToEngage.RData', homedir))

  # test entry in DB is fine
  con <- getDBConnection(dbuser, dbpassword, dbhost, dbname, port)
  con_l <- getDBConnectionLearning(dbuser, dbpassword, dbhost, dbname, port)

  learningRun <- dbGetQuery(con_l, 'SELECT * from LearningRun;')
  learningBuild <- dbGetQuery(con_l, 'SELECT * from LearningBuild;')

  accountTimeToEngage <- data.table(dbGetQuery(con, 'SELECT * FROM AccountTimeToEngage ORDER BY accountId, repActionTypeId desc, predictionDate'))
  accountTimeToEngage[, predictionDate := as.Date(predictionDate)]

  dbDisconnect(con)
  dbDisconnect(con_l)

  expect_equal(dim(learningBuild), c(0,9))
  expect_equal(dim(learningRun), c(1,10))
  expect_setequal(unname(unlist(learningRun[,c('learningBuildUID','isPublished','runType','executionStatus')])), c(buildUID,1,'TTE','success'))

  # get runUID
  runUID <- learningRun[learningRun$learningBuildUID==buildUID,"learningRunUID"]

  # test save resutls in DB
  expect_equal(dim(accountTimeToEngage), c(18540, 10))
  expect_length(unique(accountTimeToEngage$tteAlgorithmName), 1)
  expect_equal(dim(accountTimeToEngage[accountTimeToEngage$learningRunUID==runUID,]), c(18540, 10))

  # test the saved scores in DB is the same as the one saved locally
  #accountTimeToEngage <- data.table(accountTimeToEngage)
  #setorder(accountTimeToEngage, accountId)
  #setorder(savedScores, accountId)
  #expect_equal(unname(data.frame(accountTimeToEngage[accountTimeToEngage$learningRunUID==runUID,c("accountId", "method")])), unname(data.frame(savedScores[,c("accountId", "method")])))

  scores_tte_dse[, c("learningRunUID", "runDate", "createdAt", "updatedAt", "probability", "tteAlgorithmName") := NULL]
  accountTimeToEngage[, c("learningRunUID", "runDate", "createdAt", "updatedAt", "probability", "tteAlgorithmName") := NULL]
  expect_equal(scores_tte_dse, accountTimeToEngage, ignore.col.order = T, check.attribute = F)

  # check build dir structure
  expect_file_exists(sprintf('%s/builds/%s',homedir,buildUID))
  expect_file_exists(sprintf('%s/builds/%s/plot_output_%s',homedir,buildUID,paste("Score",runUID,sep="_")))

  # run tests on log file contents
  # read log files for info (func in source(sprintf('%s/common/unitTest/utilsFunc.r',homedir)))
  log_contents <- readLogFile(homedir,buildUID,paste("Score",runUID,sep="_"))
  # read config for result comparison
  propertiesFilePath <- sprintf("%s/builds/%s/learning.properties",homedir,buildUID)
  config <- read.properties(propertiesFilePath)

  # check general log information
  ind <- grep('Result\\[\\[1\\]\\]= 0', log_contents)
  expect_gt(ind, 0)

  ind <- grep('entered saveTimingScoreResult function', log_contents)
  expect_gt(ind, 0)

  ind <- grep('Back from MT scoring and score save', log_contents)
  expect_gt(ind, 0)

})

test_that('test messageTimingScoreDriver for TTE Reporting ...', {
  # run script
  isNightly <- FALSE
  runmodel <<- "REPORT"

  runMessageTimingScoreDriverMockDataReset(isNightly, buildUID)
  runUID <- runMessageTimingScoreDriver(isNightly, buildUID)

  # test cases
  # load saved manual scoring data for comparison
  load(sprintf('%s/messageTiming/tests/data/from_TTE_scoresReport.RData', homedir)) # scores
  load(sprintf('%s/messageTiming/tests/data/from_TTE_scoresSegReport.RData', homedir)) # scoresSeg

  # test entry in DB is fine
  con <- getDBConnection(dbuser, dbpassword, dbhost, dbname, port)
  con_l <- getDBConnectionLearning(dbuser, dbpassword, dbhost, dbname, port)

  learningRun <- dbGetQuery(con_l, 'SELECT * from LearningRun;')

  accountTimeToEngage.rpt <- data.table(dbGetQuery(con_l, 'SELECT * from AccountTimeToEngageReport ORDER BY accountId, repActionTypeId desc, method, intervalOrDoW'))
  segmentTimeToEngage.rpt <- data.table(dbGetQuery(con_l, 'SELECT * from SegmentTimeToEngageReport ORDER BY segment, segmentType, channelUID, method, intervalOrDoW'))

  setorder(scores, accountId, -repActionTypeId, method, intervalOrDoW)
  setorder(scoresSeg, segment, segmentType, channelUID, method, intervalOrDoW)

  dbDisconnect(con)
  dbDisconnect(con_l)

  expect_equal(dim(learningRun), c(1,10))
  expect_equal(unname(unlist(learningRun[,c('learningRunUID','learningBuildUID','isPublished','runType','executionStatus')])), c(runUID,buildUID,0,'TTE','success'))
  expect_equal(learningRun$updatedAt>=learningRun$createdAt, TRUE)
  expect_equal(dim(accountTimeToEngage.rpt), c(21630, 10))
  expect_equal(dim(segmentTimeToEngage.rpt), c(2870, 10))

  # test scores are the same as saved data
  scores[, c("learningRunUID", "runDate", "createdAt", "updatedAt", "probability") := NULL]
  accountTimeToEngage.rpt[, c("learningRunUID", "runDate", "createdAt", "updatedAt", "probability", "tteAlgorithmName") := NULL]
  expect_equal(scores, accountTimeToEngage.rpt, ignore.col.order = T, check.attribute = F)

  scoresSeg[, c("learningRunUID", "createdAt", "updatedAt", "probability") := NULL]
  segmentTimeToEngage.rpt[, c("learningRunUID", "createdAt", "updatedAt", "probability", "tteAlgorithmName") := NULL]
  expect_equal(scores, accountTimeToEngage.rpt, ignore.col.order = T, check.attribute = F)

  # check build dir structure
  expect_file_exists(sprintf('%s/builds/%s',homedir,buildUID))
  expect_file_exists(sprintf('%s/builds/%s/plot_output_%s',homedir,buildUID,paste("Score",RUN_UID,sep="_")))

  # run tests on log file contents
  # read log files for info (func in source(sprintf('%s/common/unitTest/utilsFunc.r',homedir)))
  log_contents <- readLogFile(homedir,buildUID,paste("Score",runUID,sep="_"))

  # check general log information
  ind <- grep('Running reporting job', log_contents)
  expect_gt(ind, 0)

  ind <- grep('entered saveTimingScoreReport function', log_contents)
  expect_gt(ind, 0)

  ind <- grep('Back from MT scoring and score save', log_contents)
  expect_gt(ind, 0)
})
