#!/usr/bin/env python
# encoding: utf-8
import os
import sys
import requests
import json
import boto3
import mysql.connector
from mysql.connector import Error
from boto3.session import Session


customer = sys.argv[1]
environment = sys.argv[2]
jobName = 'ADL_' + customer + '_' + environment
cloningPath = sys.argv[3] + '/learning/adl/code/'
ACCESS_KEY = ''
SECRET_KEY = ''
awsRegion = ''
s3Destination = ''
rptS3Location = ''
environmentdb = ''
iamRole = ''
mappingLocation = ''
role = 'Aktana-AWSGlueServiceRole-Global'  # Read from CustomerADLConfig table in future
description = 'Deployment of a glue job for ADL'
maxConcurrentRuns = 2


def copyADLCodeToS3(source, destination):
    s3Client = boto3.client('s3')
    arr = os.listdir(source)
    for f in arr:
        try:
            response = s3Client.upload_file(source + f, destination, "adl/common/code/" + f)
        except requests.exceptions.HTTPError as e:
            print(e.response.text)
            exit(1)


if __name__ == "__main__":
    # get CustomerADLConfig data
    file_path = sys.argv[3] + '/learning/adl'
    os.chdir(file_path)
    with open('customer-metadata.json', 'r') as f:
        config = json.load(f)

    try:
        connection = mysql.connector.connect(host=config["host"], database=config["database"], user=config["username"],
                                             password=config["password"])
        if connection.is_connected():
            cursor = connection.cursor()
            query = "select b.`adlS3Location`, b.`awsRegion`, b.`awsAccessKey`, b.`awsSecretKey`, b.`rptS3Location`, b.`environment`, b.`iamRole`, b.`mappingLocation` from `Customer` a join `CustomerADLConfig` b on a.customerId = b.customerId where a.`customerName`='{}' and b.`environment`='{}'".format(
                customer, environment)
            cursor.execute(query)
            record = cursor.fetchall()
            if (len(record) == 1):
                s3Destination = record[0][0]
                awsRegion = record[0][1]
                ACCESS_KEY = record[0][2]
                SECRET_KEY = record[0][3]
                rptS3Location = record[0][4]
                environmentdb = record[0][5]
                iamRole = record[0][6]
                mappingLocation = record[0][7]
            else:
                print('Customer Information not found in CustomerADLConfig table')
                exit(1)
    except Error as e:
        print("Error", e)
        exit(1)
    finally:
        # closing database connection.
        if (connection.is_connected()):
            cursor.close()
            connection.close()


    session = Session(aws_access_key_id=ACCESS_KEY, aws_secret_access_key=SECRET_KEY, region_name=awsRegion)
    s3CustomerAdl = s3Destination

    # Copy code to appropriate S3 bucket
    s3Destination = s3Destination.split('/adl/', 1)[0]
    s3Destination = s3Destination.replace('s3://', '')
    copyADLCodeToS3(cloningPath, s3Destination)
    print("code copied to " + s3Destination + "/adl/common/code")

    # Set extra job parameters to appropriate relative paths
    tempDir = s3CustomerAdl + 'logs/'
    jars = 's3://' + s3Destination + '/adl/common/lib/delta-core_2.11-0.5.0.jar'
    scriptLocation = 's3://' + s3Destination + '/adl/common/code/adlDriver.py'
    pyfiles = 's3://' + s3Destination + '/adl/common/code/dataLoading.py,s3://' + s3Destination + '/adl/common/code/utils.py'
    configs = 's3://' + s3Destination + '/adl/common/code/config.json'

    # Create the Glue job
    try:
        client = session.client('glue')
        response = client.create_job(
            Name=jobName,
            Description=description,
            LogUri='string',
            Role=role,
            ExecutionProperty={
                'MaxConcurrentRuns': maxConcurrentRuns
            },
            Command={
                'Name': 'glueetl',
                'ScriptLocation': scriptLocation,
                'PythonVersion': '3'
            },
            DefaultArguments={
                '--customer': customer,
                '--environment': environment,
                '--accesKey': ACCESS_KEY,
                '--secreteKey': SECRET_KEY,
                '--rptS3Location': rptS3Location,
                '--s3CustomerAdl': s3CustomerAdl,
                '--mappingLocation': mappingLocation,
                '--extra-jars': jars,
                '--extra-py-files': pyfiles,
                '--extra-files': configs,
                '--TempDir': tempDir,
                '--job-bookmark-option': 'job-bookmark-disable'
            },
            GlueVersion='1.0',
            MaxRetries=3,
            Timeout=240,
            MaxCapacity=10.0,
            # NumberOfWorkers=5,
            # WorkerType='Standard'
        )

        if response["ResponseMetadata"]["HTTPStatusCode"] == 200:
            print(jobName + " Job Created Successfully")

        else:
            print(response)
            exit(1)

    except Exception as e:
        print(e)
        exit(1)
