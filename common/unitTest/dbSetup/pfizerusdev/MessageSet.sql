CREATE TABLE `MessageSet` (
  `messageSetId` int(11) NOT NULL AUTO_INCREMENT,
  `productId` int(11) NOT NULL,
  `messageSetName` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `messageSetDescription` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `externalId` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `messageAlgorithmId` int(11) DEFAULT NULL,
  `messageAlgorithmAccuracy` double DEFAULT '0',
  `defaultToRotatingSelector` int(1) DEFAULT NULL,
  `createdAt` datetime DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `repActionChannelId` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`messageSetId`),
  UNIQUE KEY `messageSet_uniqueExternalId` (`externalId`),
  UNIQUE KEY `messageSet_uniqueName` (`messageSetName`),
  KEY `messageSet_externalId` (`externalId`),
  KEY `messageSet_productId` (`productId`)
) ENGINE=InnoDB AUTO_INCREMENT=1020 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci