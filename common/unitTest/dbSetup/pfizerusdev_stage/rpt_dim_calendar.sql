CREATE TABLE `rpt_dim_calendar` (
  `datefield` date NOT NULL DEFAULT '1000-01-01',
  `day_of_week` int(1) DEFAULT NULL,
  `calendar_year` int(4) DEFAULT NULL,
  `calendar_quarter` int(4) DEFAULT NULL,
  `quarter_value` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `month_of_year` int(4) DEFAULT NULL,
  `day_of_month` int(4) DEFAULT NULL,
  `week_label` date DEFAULT NULL,
  `week_value` date DEFAULT NULL,
  `month_value` varchar(7) CHARACTER SET utf8 DEFAULT NULL,
  `month_value_full` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `month_label` varchar(73) CHARACTER SET utf8 DEFAULT NULL,
  `is_holiday` int(11) NOT NULL DEFAULT '0',
  `is_weekend` int(11) DEFAULT NULL,
  `is_last_day_of_month` int(11) NOT NULL DEFAULT '0',
  UNIQUE KEY `datefield` (`datefield`),
  KEY `year_idx` (`calendar_year`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
