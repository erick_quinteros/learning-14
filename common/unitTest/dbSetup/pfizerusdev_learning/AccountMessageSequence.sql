CREATE TABLE `AccountMessageSequence` (
  `learningRunUID` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `learningBuildUID` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `accountUID` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `messageUID` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `probability` double DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  KEY `AccountMessageSequence_index` (`learningRunUID`,`accountUID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci