from pyspark.sql.types import *

# schema = StructType([StructField("runRepDateId",StringType(), nullable = True),
#         StructField("runId",StringType(), nullable = True),
#         StructField("repId",StringType(), nullable = True),
#         StructField("repUID",StringType(), nullable = True),
#         StructField("suggestedDate",DateType(), nullable = True)])

dserun = StructType([StructField("runId",StringType(), nullable = True),
        StructField("runUID",StringType(), nullable = True),
        StructField("startDateTime",DateType(), nullable = True),
        StructField("startDateLocal",DateType(), nullable = True),
        StructField("seConfigId",StringType(), nullable = True),
        StructField("runGroupId",StringType(), nullable = True),
        StructField("persistenceVerbosity",StringType(), nullable = True),
        StructField("runSeriesName",StringType(), nullable = True),
        StructField("repId",StringType(), nullable = True),
        StructField("numSuggestibleReps",StringType(), nullable = True),
        StructField("numSuggestibleAccounts",StringType(), nullable = True),
        StructField("numEvaluatedAccounts",StringType(), nullable = True),
        StructField("numErrorsAndWarnings",StringType(), nullable = True),
        StructField("elapsedSecsQueries",StringType(), nullable = True),
        StructField("elapsedSecsCompute",StringType(), nullable = True),
        StructField("elapsedSecsPersist",StringType(), nullable = True),
        StructField("elapsedSecsTotal",StringType(), nullable = True),
        StructField("runSummaryReport",StringType(), nullable = True)])

dseRunRepDate = StructType([StructField("runRepDateId",StringType(), nullable = True),
        StructField("runId",StringType(), nullable = True),
        StructField("repId",StringType(), nullable = True),
        StructField("repUID",StringType(), nullable = True),
        StructField("suggestedDate",DateType(), nullable = True)])

dseRunRepDateSPARK = StructType([StructField("runRepDateId",StringType(), nullable = True),
        StructField("runUID",StringType(), nullable = True),
        StructField("repId",StringType(), nullable = True),
        StructField("repUID",StringType(), nullable = True),
        StructField("suggestedDate",DateType(), nullable = True)])

final_df_one = StructType([StructField("runId",StringType(), nullable = True),
        StructField("startDateLocal",DateType(), nullable = True),
        StructField("repId",StringType(), nullable = True),
        StructField("startDateTime",DateType(), nullable = True),
        StructField("publishedDate",StringType(), nullable = True)])

final_df_oneSPARK = StructType([StructField("runUID",StringType(), nullable = True),
        StructField("startDateLocal",DateType(), nullable = True),
        StructField("repId",StringType(), nullable = True),
        StructField("startDateTime",DateType(), nullable = True),
        StructField("publishedDate",StringType(), nullable = True)])

nextPublished = StructType([StructField("repId",StringType(), nullable = True),
        StructField("runId",StringType(), nullable = True),
        StructField("publishedDate",StringType(), nullable = True),
        StructField("startDateLocal",DateType(), nullable = True),
        StructField("nextPublishedDate",StringType(), nullable = True)])

nextPublishedSPARK = StructType([StructField("repId",StringType(), nullable = True),
        StructField("runUID",StringType(), nullable = True),
        StructField("publishedDate",StringType(), nullable = True),
        StructField("startDateLocal",DateType(), nullable = True),
        StructField("nextPublishedDate",StringType(), nullable = True)])

rep = StructType([StructField("repId",StringType(), nullable = True),
        StructField("repTypeId",StringType(), nullable = True),
        StructField("workWeekId",StringType(), nullable = True),
        StructField("externalId",StringType(), nullable = True),
        StructField("repName",StringType(), nullable = True),
        StructField("latitude",StringType(), nullable = True),
        StructField("longitude",StringType(), nullable = True),
        StructField("timeZoneId",StringType(), nullable = True),
        StructField("isActivated",StringType(), nullable = True),
        StructField("isDeleted",StringType(), nullable = True),
        StructField("seConfigId",StringType(), nullable = True),
        StructField("createdAt",StringType(), nullable = True),
        StructField("updatedAt",StringType(), nullable = True)])

next_published_tot = StructType([StructField("repId",StringType(), nullable = True),
        StructField("externalId",StringType(), nullable = True),
        StructField("runId",StringType(), nullable = True),
        StructField("publishedDate",StringType(), nullable = True),
        StructField("startDateLocal",DateType(), nullable = True),
        StructField("nextPublishedDate",StringType(), nullable = True)])

sync_tracking = StructType([StructField("Id",StringType(), nullable = True),
        StructField("OwnerId",StringType(), nullable = True),
        StructField("IsDeleted",StringType(), nullable = True),
        StructField("Name",StringType(), nullable = True),
        StructField("CreatedDate",TimestampType(), nullable = True),
        StructField("CreatedById",StringType(), nullable = True),
        StructField("LastModifiedDate",StringType(), nullable = True),
        StructField("LastModifiedById",StringType(), nullable = True),
        StructField("SystemModstamp",StringType(), nullable = True),
        StructField("MayEdit",StringType(), nullable = True),
        StructField("IsLocked",StringType(), nullable = True),
        StructField("LastViewedDate",StringType(), nullable = True),
        StructField("LastReferencedDate",StringType(), nullable = True),
        StructField("ConnectionReceivedId",StringType(), nullable = True),
        StructField("ConnectionSentId",StringType(), nullable = True),
        StructField("Download_Processed_vod__c",StringType(), nullable = True),
        StructField("Mobile_ID_vod__c",StringType(), nullable = True),
        StructField("Successful_Sync_vod__c",StringType(), nullable = True),
        StructField("Sync_Completed_Datetime_vod__c",StringType(), nullable = True),
        StructField("Sync_Duration_vod__c",StringType(), nullable = True),
        StructField("Sync_Start_Datetime_vod__c",StringType(), nullable = True),
        StructField("Sync_Type_vod__c",StringType(), nullable = True),
        StructField("Upload_Processed_vod__c",StringType(), nullable = True),
        StructField("VInsights_Processed_vod__c",StringType(), nullable = True),
        StructField("Version_vod__c",StringType(), nullable = True),
        StructField("Media_Processed_vod__c",StringType(), nullable = True),
        StructField("fOwner_Profile_coe__c",StringType(), nullable = True),
        StructField("Record_Created_DateTime__c",StringType(), nullable = True),
        StructField("Discrepancy_Secs__c",StringType(), nullable = True),
        StructField("Canceled_vod__c",StringType(), nullable = True),
        StructField("Sync_Discrepancy_Duration_Delta__c",StringType(), nullable = True),
        StructField("Number_of_Retries_vod__c",StringType(), nullable = True),
        StructField("Number_of_Upload_Errors_vod__c",StringType(), nullable = True),
        StructField("Number_of_Uploads_vod__c",StringType(), nullable = True),
        StructField("Number_of_VTrans_vod__c",StringType(), nullable = True),
        StructField("BackupCreatedDate",StringType(), nullable = True),
        StructField("BackupModifiedDate",StringType(), nullable = True)])

sync_info = StructType([StructField("externalId",StringType(), nullable = True),
        StructField("syncStart",TimestampType(), nullable = True)])

synced_runs = StructType([StructField("repId",StringType(), nullable = True),
        StructField("StartDateLocalSynched",DateType(), nullable = True),
        StructField("runId",StringType(), nullable = True),
        StructField("Times_Synched",DoubleType(), nullable = True)])

suggestion_delivered_raw = StructType([StructField("externalId",StringType(), nullable = True),
        StructField("suggestionLifeCycleId",StringType(), nullable = True),
        StructField("runId",StringType(), nullable = True),
        StructField("runUID",StringType(), nullable = True),
        StructField("repTeamId",StringType(), nullable = True),
        StructField("repTeamUID",StringType(), nullable = True),
        StructField("repTeamName",StringType(), nullable = True),
        StructField("seConfigId",StringType(), nullable = True),
        StructField("seConfigName",StringType(), nullable = True),
        StructField("runGroupId",StringType(), nullable = True),
        StructField("suggestedDate",DateType(), nullable = True),
        StructField("startDateLocal",DateType(), nullable = True),
        StructField("repId",StringType(), nullable = True),
        StructField("repUID",StringType(), nullable = True),
        StructField("repName",StringType(), nullable = True),
        StructField("repCreatedAt",StringType(), nullable = True),
        StructField("accountId",StringType(), nullable = True),
        StructField("accountUID",StringType(), nullable = True),
        StructField("runRepDateSuggestionId",StringType(), nullable = True),
        StructField("accountName",StringType(), nullable = True),
        StructField("detailRepActionTypeId",StringType(), nullable = True),
        StructField("detailRepActionTypeUID",StringType(), nullable = True),
        StructField("detailRepActionName",StringType(), nullable = True),
        StructField("runRepDateSuggestionDetailId",StringType(), nullable = True),
        StructField("productId",StringType(), nullable = True),
        StructField("productUID",StringType(), nullable = True),
        StructField("productName",StringType(), nullable = True),
        StructField("messageId",StringType(), nullable = True),
        StructField("messageUID",StringType(), nullable = True),
        StructField("messageName",StringType(), nullable = True),
        StructField("suggestionUID",StringType(), nullable = True),
        StructField("suggestionReferenceId",StringType(), nullable = True),
        StructField("lastViewedAt",StringType(), nullable = True),
        StructField("viewedAt",StringType(), nullable = True),
        StructField("viewedDuration",StringType(), nullable = True),
        StructField("actionTaken",StringType(), nullable = True),
        StructField("actionTaken_dt",StringType(), nullable = True),
        StructField("isSuggestionCompleted",StringType(), nullable = True),
        StructField("isSuggestionCompletedDirect",StringType(), nullable = True),
        StructField("isSuggestionCompletedInfer",StringType(), nullable = True),
        StructField("isSuggestionDismissed",StringType(), nullable = True),
        StructField("dismissReasonType",StringType(), nullable = True),
        StructField("dismissReason",StringType(), nullable = True),
        StructField("dismissReason_dt",StringType(), nullable = True),
        StructField("isSuggestionActive",StringType(), nullable = True),
        StructField("facilityLatitude",StringType(), nullable = True),
        StructField("facilityLongitude",StringType(), nullable = True),
        StructField("facilityGeoLocationString",StringType(), nullable = True),
        StructField("repLatitude",StringType(), nullable = True),
        StructField("repLongitude",StringType(), nullable = True),
        StructField("territoryCityName",StringType(), nullable = True),
        StructField("districtName",StringType(), nullable = True),
        StructField("territoryId",StringType(), nullable = True),
        StructField("territoryName",StringType(), nullable = True),
        StructField("regionName",StringType(), nullable = True),
        StructField("regionGroup",StringType(), nullable = True),
        StructField("dismissedAt",StringType(), nullable = True),
        StructField("dismissCount",StringType(), nullable = True),
        StructField("lastPublishedAt",StringType(), nullable = True),
        StructField("createdAt",DateType(), nullable = True),
        StructField("updatedAt",DateType(), nullable = True),
        StructField("reportedInteractionUID",StringType(), nullable = True),
        StructField("inferredInteractionUID",StringType(), nullable = True),
        StructField("interactionUID",StringType(), nullable = True),
        StructField("completedAt",StringType(), nullable = True),
        StructField("inferredAt",StringType(), nullable = True),
        StructField("isFirstSuggestedDate",StringType(), nullable = True),
        StructField("isLastSuggestedDate",StringType(), nullable = True),
        StructField("suggestionDriver",StringType(), nullable = True),
        StructField("timeoffday",StringType(), nullable = True),
        StructField("isHighestRunidForDay",StringType(), nullable = True),
        StructField("holiday_weekend_Flag",StringType(), nullable = True),
        StructField("crmFieldName",StringType(), nullable = True),
        StructField("reasonText",StringType(), nullable = True),
        StructField("reasonRank",StringType(), nullable = True),
        StructField("runRepDateSuggestionReasonId",StringType(), nullable = True),
        StructField("repRole",StringType(), nullable = True),
        StructField("repBag",StringType(), nullable = True),
        StructField("dmUID",StringType(), nullable = True),
        StructField("dmName",StringType(), nullable = True),
        StructField("interactionId",StringType(), nullable = True),
        StructField("isCompleted",StringType(), nullable = True),
        StructField("startDateTime",DateType(), nullable = True),
        StructField("isREMix",StringType(), nullable = True),
        StructField("isDSESpark",StringType(), nullable = True),
        StructField("arc_createdAt",DateType(), nullable = True),
        StructField("arc_updatedAt",DateType(), nullable = True)])

suggestion_Delivered_synced = StructType([StructField("repId",StringType(), nullable = True),
        StructField("runId",StringType(), nullable = True),
        StructField("externalId",StringType(), nullable = True),
        StructField("suggestionLifeCycleId",StringType(), nullable = True),
        StructField("runUID",StringType(), nullable = True),
        StructField("repTeamId",StringType(), nullable = True),
        StructField("repTeamUID",StringType(), nullable = True),
        StructField("repTeamName",StringType(), nullable = True),
        StructField("seConfigId",StringType(), nullable = True),
        StructField("seConfigName",StringType(), nullable = True),
        StructField("runGroupId",StringType(), nullable = True),
        StructField("suggestedDate",DateType(), nullable = True),
        StructField("startDateLocal",DateType(), nullable = True),
        StructField("repUID",StringType(), nullable = True),
        StructField("repName",StringType(), nullable = True),
        StructField("repCreatedAt",StringType(), nullable = True),
        StructField("accountId",StringType(), nullable = True),
        StructField("accountUID",StringType(), nullable = True),
        StructField("runRepDateSuggestionId",StringType(), nullable = True),
        StructField("accountName",StringType(), nullable = True),
        StructField("detailRepActionTypeId",StringType(), nullable = True),
        StructField("detailRepActionTypeUID",StringType(), nullable = True),
        StructField("detailRepActionName",StringType(), nullable = True),
        StructField("runRepDateSuggestionDetailId",StringType(), nullable = True),
        StructField("productId",StringType(), nullable = True),
        StructField("productUID",StringType(), nullable = True),
        StructField("productName",StringType(), nullable = True),
        StructField("messageId",StringType(), nullable = True),
        StructField("messageUID",StringType(), nullable = True),
        StructField("messageName",StringType(), nullable = True),
        StructField("suggestionUID",StringType(), nullable = True),
        StructField("suggestionReferenceId",StringType(), nullable = True),
        StructField("lastViewedAt",StringType(), nullable = True),
        StructField("viewedAt",StringType(), nullable = True),
        StructField("viewedDuration",StringType(), nullable = True),
        StructField("actionTaken",StringType(), nullable = True),
        StructField("actionTaken_dt",StringType(), nullable = True),
        StructField("isSuggestionCompleted",StringType(), nullable = True),
        StructField("isSuggestionCompletedDirect",StringType(), nullable = True),
        StructField("isSuggestionCompletedInfer",StringType(), nullable = True),
        StructField("isSuggestionDismissed",StringType(), nullable = True),
        StructField("dismissReasonType",StringType(), nullable = True),
        StructField("dismissReason",StringType(), nullable = True),
        StructField("dismissReason_dt",StringType(), nullable = True),
        StructField("isSuggestionActive",StringType(), nullable = True),
        StructField("facilityLatitude",StringType(), nullable = True),
        StructField("facilityLongitude",StringType(), nullable = True),
        StructField("facilityGeoLocationString",StringType(), nullable = True),
        StructField("repLatitude",StringType(), nullable = True),
        StructField("repLongitude",StringType(), nullable = True),
        StructField("territoryCityName",StringType(), nullable = True),
        StructField("districtName",StringType(), nullable = True),
        StructField("territoryId",StringType(), nullable = True),
        StructField("territoryName",StringType(), nullable = True),
        StructField("regionName",StringType(), nullable = True),
        StructField("regionGroup",StringType(), nullable = True),
        StructField("dismissedAt",StringType(), nullable = True),
        StructField("dismissCount",StringType(), nullable = True),
        StructField("lastPublishedAt",StringType(), nullable = True),
        StructField("createdAt",DateType(), nullable = True),
        StructField("updatedAt",DateType(), nullable = True),
        StructField("reportedInteractionUID",StringType(), nullable = True),
        StructField("inferredInteractionUID",StringType(), nullable = True),
        StructField("interactionUID",StringType(), nullable = True),
        StructField("completedAt",StringType(), nullable = True),
        StructField("inferredAt",StringType(), nullable = True),
        StructField("isFirstSuggestedDate",StringType(), nullable = True),
        StructField("isLastSuggestedDate",StringType(), nullable = True),
        StructField("suggestionDriver",StringType(), nullable = True),
        StructField("timeoffday",StringType(), nullable = True),
        StructField("isHighestRunidForDay",StringType(), nullable = True),
        StructField("holiday_weekend_Flag",StringType(), nullable = True),
        StructField("crmFieldName",StringType(), nullable = True),
        StructField("reasonText",StringType(), nullable = True),
        StructField("reasonRank",StringType(), nullable = True),
        StructField("runRepDateSuggestionReasonId",StringType(), nullable = True),
        StructField("repRole",StringType(), nullable = True),
        StructField("repBag",StringType(), nullable = True),
        StructField("dmUID",StringType(), nullable = True),
        StructField("dmName",StringType(), nullable = True),
        StructField("interactionId",StringType(), nullable = True),
        StructField("isCompleted",StringType(), nullable = True),
        StructField("startDateTime",DateType(), nullable = True),
        StructField("isREMix",StringType(), nullable = True),
        StructField("isDSESpark",StringType(), nullable = True),
        StructField("arc_createdAt",DateType(), nullable = True),
        StructField("arc_updatedAt",DateType(), nullable = True),
        StructField("StartDateLocalSynched",DateType(), nullable = True),
        StructField("Times_Synched",DoubleType(), nullable = True)])

repLicenceTable = StructType([StructField("cluster",StringType(), nullable = True),
        StructField("districtName",StringType(), nullable = True),
        StructField("repName",StringType(), nullable = True),
        StructField("territoryName",StringType(), nullable = True),
        StructField("territoryId",StringType(), nullable = True),
        StructField("externalId",StringType(), nullable = True),
        StructField("customerId",StringType(), nullable = True),
        StructField("regionName",StringType(), nullable = True),
        StructField("activatedDate",StringType(), nullable = True),
        StructField("isActivated",StringType(), nullable = True),
        StructField("createdAt",StringType(), nullable = True),
        StructField("updatedAt",StringType(), nullable = True),
        StructField("repRole",StringType(), nullable = True),
        StructField("repBag",StringType(), nullable = True),
        StructField("seConfigId",StringType(), nullable = True),
        StructField("testRunGroupId",StringType(), nullable = True),
        StructField("RunGroupId",StringType(), nullable = True),
        StructField("dmName",StringType(), nullable = True),
        StructField("dmUID",StringType(), nullable = True),
        StructField("startDate",StringType(), nullable = True),
        StructField("endDate",StringType(), nullable = True)])

SuggDel_RepLicence = StructType([StructField("startDateLocal",DateType(), nullable = True),
        StructField("repUID",StringType(), nullable = True),
        StructField("repName",StringType(), nullable = True),
        StructField("cluster",StringType(), nullable = True),
        StructField("seConfigId",StringType(), nullable = True),
        StructField("seConfigName",StringType(), nullable = True),
        StructField("suggestionDriver",StringType(), nullable = True),
        StructField("suggestionReferenceId",StringType(), nullable = True),
        StructField("territoryId",StringType(), nullable = True),
        StructField("territoryName",StringType(), nullable = True),
        StructField("suggestedDate",DateType(), nullable = True),
        StructField("actionTaken",StringType(), nullable = True)])

dim_calendar = StructType([StructField("datefield",DateType(), nullable = True),
        StructField("day_of_week",StringType(), nullable = True),
        StructField("calendar_year",StringType(), nullable = True),
        StructField("calendar_quarter",StringType(), nullable = True),
        StructField("quarter_value",StringType(), nullable = True),
        StructField("month_of_year",StringType(), nullable = True),
        StructField("day_of_month",StringType(), nullable = True),
        StructField("week_label",DateType(), nullable = True),
        StructField("week_value",DateType(), nullable = True),
        StructField("month_value",StringType(), nullable = True),
        StructField("month_value_full",StringType(), nullable = True),
        StructField("month_label",StringType(), nullable = True),
        StructField("is_holiday",StringType(), nullable = True),
        StructField("is_weekend",StringType(), nullable = True),
        StructField("is_last_day_of_month",StringType(), nullable = True)])

suggestions_df1 = StructType([StructField("startDateLocal",DateType(), nullable = True),
        StructField("repUID",StringType(), nullable = True),
        StructField("repName",StringType(), nullable = True),
        StructField("cluster",StringType(), nullable = True),
        StructField("seConfigId",StringType(), nullable = True),
        StructField("seConfigName",StringType(), nullable = True),
        StructField("suggestionDriver",StringType(), nullable = True),
        StructField("suggestionReferenceId",StringType(), nullable = True),
        StructField("territoryId",StringType(), nullable = True),
        StructField("territoryName",StringType(), nullable = True),
        StructField("suggestedDate",DateType(), nullable = True),
        StructField("actionTaken",StringType(), nullable = True),
        StructField("datefield",DateType(), nullable = True),
        StructField("day_of_week",StringType(), nullable = True),
        StructField("calendar_year",StringType(), nullable = True),
        StructField("calendar_quarter",StringType(), nullable = True),
        StructField("quarter_value",StringType(), nullable = True),
        StructField("month_of_year",StringType(), nullable = True),
        StructField("day_of_month",StringType(), nullable = True),
        StructField("week_label",DateType(), nullable = True),
        StructField("week_value",DateType(), nullable = True),
        StructField("month_value",StringType(), nullable = True),
        StructField("month_value_full",StringType(), nullable = True),
        StructField("month_label",StringType(), nullable = True),
        StructField("is_holiday",StringType(), nullable = True),
        StructField("is_weekend",StringType(), nullable = True),
        StructField("is_last_day_of_month",StringType(), nullable = True)])

suggestions_df2 = StructType([StructField("year",IntegerType(), nullable = True),
        StructField("month",IntegerType(), nullable = True),
        StructField("repUID",StringType(), nullable = True),
        StructField("repName",StringType(), nullable = True),
        StructField("seConfigId",StringType(), nullable = True),
        StructField("seConfigName",StringType(), nullable = True),
        StructField("repTeamUID",StringType(), nullable = True),
        StructField("repTeamName",StringType(), nullable = True),
        StructField("suggestionType",StringType(), nullable = True),
        StructField("suggestionReferenceId",StringType(), nullable = True),
        StructField("territoryId",StringType(), nullable = True),
        StructField("territoryName",StringType(), nullable = True),
        StructField("suggestionsDelivered",LongType(), nullable = True),
        StructField("actionTaken",StringType(), nullable = True)])

suggestion_delivered = StructType([StructField("year",IntegerType(), nullable = True),
        StructField("month",IntegerType(), nullable = True),
        StructField("repUID",StringType(), nullable = True),
        StructField("repName",StringType(), nullable = True),
        StructField("seConfigId",StringType(), nullable = True),
        StructField("seConfigName",StringType(), nullable = True),
        StructField("suggestionType",StringType(), nullable = True),
        StructField("territoryId",StringType(), nullable = True),
        StructField("territoryName",StringType(), nullable = True),
        StructField("totalSuggestionsDeliveredTimes",LongType(), nullable = True)])

engaged_suggestions = StructType([StructField("year",IntegerType(), nullable = True),
        StructField("month",IntegerType(), nullable = True),
        StructField("repUID",StringType(), nullable = True),
        StructField("repName",StringType(), nullable = True),
        StructField("seConfigId",StringType(), nullable = True),
        StructField("seConfigName",StringType(), nullable = True),
        StructField("suggestionType",StringType(), nullable = True),
        StructField("territoryId",StringType(), nullable = True),
        StructField("territoryName",StringType(), nullable = True),
        StructField("engagedUniqueSuggestionsCount",LongType(), nullable = True)])

final_df = StructType([StructField("year",IntegerType(), nullable = True),
        StructField("month",IntegerType(), nullable = True),
        StructField("repUID",StringType(), nullable = True),
        StructField("repName",StringType(), nullable = True),
        StructField("seConfigId",StringType(), nullable = True),
        StructField("seConfigName",StringType(), nullable = True),
        StructField("suggestionType",StringType(), nullable = True),
        StructField("territoryId",StringType(), nullable = True),
        StructField("territoryName",StringType(), nullable = True),
        StructField("totalSuggestionsDeliveredTimes",LongType(), nullable = True),
        StructField("engagedUniqueSuggestionsCount",LongType(), nullable = True)])

RepEngagementCalculation = StructType([StructField("year", StringType(), nullable = True),
        StructField("month", StringType(), nullable = True),
        StructField("repUID", StringType(), nullable = True),
        StructField("repName", StringType(), nullable = True),
        StructField("repTeamUID", StringType(), nullable = True),
        StructField("repTeamName", StringType(), nullable = True),
        StructField("seConfigId", StringType(), nullable = True),
        StructField("seConfigName", StringType(), nullable = True),
        StructField("suggestionType", StringType(), nullable = True),
        StructField("territoryId", StringType(), nullable = True),
        StructField("territoryName", StringType(), nullable = True),
        StructField("engagedUniqueSuggestionsCount", StringType(), nullable = True),
        StructField("totalSuggestionsDeliveredTimes", DoubleType(), nullable = True),
        StructField("createdAt", TimestampType(), nullable = True),
        StructField("updatedAt", TimestampType(), nullable = True)])
