from configparser import ConfigParser
import os

CONFIG_DEFAULT_SECTION = "default_section"


class LearningPropertiesReader(ConfigParser):
    """
    This class is responsible for reading and accessing the learning.properties file.
    TO-DO: Move this to common
    """

    def read_learning_properties(self, learning_properties_file_path):
        """
        This funciton reads the `learning.properties` file from existing models with default section
        :param learning_properties_file:
        :return:
        """
        assert os.path.isfile(learning_properties_file_path), "Learning properties file does not exists"

        with open(learning_properties_file_path, 'r') as fp:
            config_string = '[' + CONFIG_DEFAULT_SECTION + ']\n' + fp.read()
            self.read_string(config_string)

    def get_property(self, property_name):
        """
        This will return the value for the property name specified
        :param property_name:
        :return:
        """
        return self.get(CONFIG_DEFAULT_SECTION, property_name, fallback=None)
