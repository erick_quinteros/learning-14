#
#
# aktana-learning model building code for Aktana Learning Engines.
#
# description: This is initiallization code
#
#
# created by : marc.cohen@aktana.com
#
# created on : 2015-10-13
#
# Copyright AKTANA (c) 2015.
#
##########################################################
library(Learning)
library(data.table)
library(properties)
library(futile.logger)
library(RMySQL)

loadMessageSequenceData.expiredMessages <- function(con_cs, productUID) {
  flog.info("loading expired messages from _cs DB")
  expiredMessages <- dbGetQuery(con_cs,sprintf("SELECT Id FROM Approved_Document_vod__c WHERE Product_vod__c = '%s' AND Status_vod__c='Expired_vod';",productUID))
  expiredMessages <- expiredMessages[1:dim(expiredMessages)[1],1]
  print('Expired Messages:')
  print(expiredMessages)
  return(expiredMessages)
}

loadMessageSequenceData.messages <- function(messages) {
  flog.info("process messages from V3db")
  messages[,c("messageName"):=NULL]
  setnames(messages,"messageDescription","messageName")
  # messages$messageName <- iconv(messages$messageName, to='ASCII', sub="-") # handle special character like "–" etc
  return(messages)
}

loadMessageSequenceData.messageSetMessage <- function(messageSetMessage) {
  flog.info("process messageSetMessage from V3db")
  return(messageSetMessage)
}

loadMessageSequenceData.messageSet <- function(messageSet) {
  flog.info("process messageSet from V3db")
  return(messageSet)
}

loadMessageSequenceData.accountPredictorNames <- function(accounts) {
  flog.info("save accountPredictorNames from accounts from V3db")
  accountPredictorNames <- names(accounts)[!(names(accounts) %in% c("externalId","createdAt","updatedAt","facilityId","accountName","isDeleted","accountId"))]
  accountPredictorNames <- gsub("-","_",accountPredictorNames)
  return(accountPredictorNames)
}

loadMessageSequenceData.interactionsP <- function(con_stage, interactions) {
  flog.info("process interactions from V3db to get interactionsP (filtered interactions")
  # get delivered status information from DB
  deliveredInt <- dbGetQuery(con_stage,"SELECT DISTINCT Id FROM Sent_Email_vod__c_arc WHERE Status_vod__c = 'Delivered_vod';")
  deliveredInt <- deliveredInt[1:dim(deliveredInt)[1],1] # convert dataframe to vector
  # filter the message delivered
  flog.info('filtering internation data based on delivered_or_not information from Sent_Email_vod__c_arc')
  interactionsP <- interactions[externalId %in% deliveredInt | productInteractionTypeName=="VISIT_DETAIL"]
  interactionsP$externalId <- NULL
  # cleaning
  interactionsP[,c("facilityId","wasCreatedFromSuggestion","duration"):=NULL]
  # interactionsP is interactions before filtering by messages in messageTopic
  return(interactionsP)
}

loadMessageSequenceData.messageTopic <- function(con_l, productUID) {
  flog.info("loading messageTopic (messageClusteringResult) from _learning DB")
  messageTopic <- data.table(dbGetQuery(con_l,sprintf("SELECT * from AKT_Message_Topic_Email_Learned WHERE productUID='%s';",productUID)))
  prodNo <- which(dbGetQuery(con_l,"SELECT DISTINCT productUID from AKT_Message_Topic_Email_Learned;")$productUID==productUID)  # this is needed to be compatible with old model
  # prods <- unique(messageTopic$productUID)
  # pd <- data.table(prodNo=1:length(prods),productUID=prods)
  # messageTopic <- merge(messageTopic,pd,by="productUID")
  messageTopic[,emailTopicId:=1000*prodNo+emailTopicId]
  # messageTopic$documentDescription <- iconv(messageTopic$documentDescription, to='ASCII', sub="-") # handle special character like "–" etc
  setnames(messageTopic,"messageUID","physicalMessageUID")
  return(messageTopic)
}

loadMessageSequenceData.emailTopicNameMap <- function(messageTopic) {
  flog.info("compose emailTopicNameMap from loaded message clustering result (messageTopic)")
  emailTopicNameMap <- unique(messageTopic[,c("emailTopicId","emailTopicName")])
  emailTopicNameMap <- setNames(as.list(emailTopicNameMap$emailTopicName), emailTopicNameMap$emailTopicId)
  return(emailTopicNameMap)
}

loadMessageSequenceData.events <- function(events) {
  flog.info("process events from V3db")
  # filter only open and click
  eventsP <- events[eventTypeName %in% c("RTE_OPEN","RTE_CLICK")]
  # change to date data type
  eventsP[,date:=as.Date(eventDate)]
  eventsP[,c("eventLabel","eventDate","eventDateTimeUTC"):=NULL]
  # cleaning
  setnames(eventsP,c("eventTypeName"),c("productInteractionTypeName"))
  return(eventsP)
}

loadMessageSequenceData.interactions <- function(interactionsP, eventsP, messageTopic) {
  flog.info("get interactions from interactionsP, eventsP, messageTopic")
  # merge events
  interactions <- rbind(interactionsP,eventsP,fill=T)
  # filter by only messages exists in messageTopic
  interactions <- interactions[!is.na(physicalMessageUID) & physicalMessageUID %in% messageTopic$physicalMessageUID,]
  return(interactions)
}

loadMessageSequenceData.emailTopicRates <- function(eventsP, messageTopic) {
  flog.info("estimate Message Topic propensities (get emailTopicRates)")
  # merge clustering results
  t <- merge(eventsP[,c("accountId","productInteractionTypeName","physicalMessageUID")],messageTopic[,c("physicalMessageUID","emailTopicId")],by="physicalMessageUID")
  t <- t[,c("accountId","productInteractionTypeName","emailTopicId"),with=F]
  t$ctr <- 1
  t[,count:=sum(ctr),by=c("accountId","productInteractionTypeName","emailTopicId")]
  t$ctr <- NULL
  t <- t[!is.na(emailTopicId)]
  t <- unique(t)
  t <- as.data.table(dcast(t,accountId~productInteractionTypeName+emailTopicId))
  # tt <- t[,colSums(is.na(t))<nrow(t)]
  # emailTopicRates <- t[,names(tt)[tt],with=F]
  # names(emailTopicRates) <- gsub("-","_",names(emailTopicRates))  # remove -'s from names
  emailTopicRates <- t
  return(emailTopicRates)
}

loadMessageSequenceData.accountProduct <- function(accountProduct, accounts, emailTopicRates) {
  flog.info("process accountProduct and merge with accounts from V3db")
  accounts[,c("externalId","createdAt","updatedAt","facilityId","accountName","isDeleted"):=NULL]
  accountProduct[,c("createdAt","updatedAt"):=NULL]
  names(accounts) <- gsub("-","_",names(accounts))  # remove -'s from names
  names(accountProduct) <- gsub("-","_",names(accountProduct))  # remove -'s from names
  # remove \n, spark cannot have \n in column name
  colClass <- sapply(accounts, class)
  charColsAccounts <- names(colClass[colClass=="character"])
  accounts <- accounts[,(charColsAccounts):=lapply(.SD, function(x) {return(gsub('[\n|\r]','',x))}),.SDcols=charColsAccounts]  # spark dataframe cannot have \n in column name
  colClass <- sapply(accountProduct, class)
  charColsAccountProduct <- names(colClass[colClass=="character"])
  accountProduct <- accountProduct[,(charColsAccountProduct):=lapply(.SD, function(x) {return(gsub('\n','',x))}),.SDcols=charColsAccountProduct]  # spark dataframe cannot have \n in column name
  # merge
  accountProduct <- merge(accountProduct,accounts,by=c("accountId"),all.x=TRUE,allow.cartesian=TRUE,suffixes = c("_x", "_y"))
  accountProduct <- merge(accountProduct,emailTopicRates,by=c("accountId"),all.x=TRUE,allow.cartesian=TRUE)
  #    accountProduct <- merge(accountProduct,stableMessageRates,by=c("accountId","productName"),all.x=TRUE,allow.cartesian=TRUE)
  flog.info("Remove bad characters")
  colClass <- sapply(accountProduct, class)
  charCols <- names(colClass[colClass=="character" & colClass!="productName"])
  replaceFunc <- function(x) {return(gsub('[[:blank:]|,|>|<]','_',x))}
  accountProduct <- cbind(accountProduct[,-..charCols],accountProduct[,..charCols][,lapply(.SD, replaceFunc)])
  return(accountProduct)
}


##########################################################################################################
#                                               Main Function
###########################################################################################################

loadMessageSequenceData <- function(con, con_l, con_stage, con_cs, productUID, readDataList=NULL)
{
  loadedData <- list()
  # constant for data needed from readV3db
  DATA_NEEDED_FROM_READV3DB <- list(interactions=c("interactions","events"), accountProduct=c("accountProduct","accounts"), products=c("products"), messageSetMessage=c("messageSetMessage"), messageSet=c("messageSet"), emailTopicNames=NULL, messages=c("messages"), messageTopic=NULL, interactionsP=c("interactions","events"), expiredMessages=NULL, accountPredictorNames=c("accounts"), emailTopicNameMap=NULL)
  # compose readDataList
  if (is.null(readDataList)) {
    readDataList <- c("interactions","accountProduct","products","messageSetMessage","messageSet","emailTopicNames","messages","messageTopic","interactionsP","expiredMessages","accountPredictorNames","emailTopicNameMap")
    flog.info("loadMessageSequenceData: all (%s)", paste(readDataList,collapse=","))
  } else {
    flog.info("loadMessageSequenceData: %s", paste(readDataList,collapse=","))
  }
  
  # use readV3dbFilter to read data
  readV3dbDataList <- NULL
  for (outputData in readDataList) {
    readV3dbDataList <- c(readV3dbDataList, DATA_NEEDED_FROM_READV3DB[[outputData]])
    readV3dbDataList <- unique(readV3dbDataList)}
  dictraw <- readV3dbFilter(con, productUID, readDataList=readV3dbDataList) 
  for(i in 1:length(dictraw))assign(names(dictraw)[i],dictraw[[i]])
  
  # load data dependent on data from readV3db but independent of other loaded data
  if ("products" %in% readDataList) {loadedData[["products"]] <- products}
  if ("messageSetMessage" %in% readDataList) {loadedData[["messageSetMessage"]] <- loadMessageSequenceData.messageSetMessage(messageSetMessage)}
  if ("messageSet" %in% readDataList) {loadedData[["messageSet"]] <- loadMessageSequenceData.messageSet(messageSet)}
  if ("accountPredictorNames" %in% readDataList) {loadedData[["accountPredictorNames"]] <- loadMessageSequenceData.accountPredictorNames(accounts)}
  
  # load data that would use data from readV3db and use for processing other loaded data
  if (any(readDataList %in% c("interactions","accountProduct","emailTopicNames","messageTopic","emailTopicNameMap"))) {messageTopic <- loadMessageSequenceData.messageTopic(con_l, productUID)}
  if (any(readDataList %in% c("interactions","accountProduct","emailTopicNames"))) {eventsP <- loadMessageSequenceData.events(events)}
  if (any(readDataList %in% c("accountProduct","emailTopicNames"))) {emailTopicRates <- loadMessageSequenceData.emailTopicRates(eventsP, messageTopic)}
  if (any(readDataList %in% c("interactionsP","interactions"))) {interactionsP <- loadMessageSequenceData.interactionsP(con_stage, interactions)}
  
  # load other data in the list of required dependent on each other
  if ("messages" %in% readDataList) {loadedData[["messages"]] <- loadMessageSequenceData.messages(messages)}
  if ("messageTopic" %in% readDataList) {loadedData[["messageTopic"]] <- messageTopic[,c("physicalMessageUID","documentDescription")]}
  if ("emailTopicNameMap" %in% readDataList) {loadedData[["emailTopicNameMap"]] <- loadMessageSequenceData.emailTopicNameMap(messageTopic)}
  if ("interactionsP" %in% readDataList) {loadedData[["interactionsP"]] <- interactionsP}
  if ("emailTopicNames" %in% readDataList) {loadedData[["emailTopicNames"]] <- names(emailTopicRates)[!names(emailTopicRates) %in% c("productName","accountId")]}
  if ("interactions" %in% readDataList) {loadedData[["interactions"]] <- loadMessageSequenceData.interactions(interactionsP, eventsP, messageTopic)}
  if ("accountProduct" %in% readDataList) {loadedData[["accountProduct"]] <- loadMessageSequenceData.accountProduct(accountProduct, accounts, emailTopicRates)}
  
  # load data independent of readV3db
  if ("expiredMessages" %in% readDataList) {loadedData[["expiredMessages"]] <- loadMessageSequenceData.expiredMessages(con_cs, productUID)}

  flog.info("Return from loadMessageSequencedata")
  return(loadedData)
}
